$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/feature/Features.feature");
formatter.feature({
  "name": "OMCC Login1",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Login to OMCC2",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Open the Browser",
  "keyword": "Given "
});
formatter.step({
  "name": "Maximize the browser",
  "keyword": "And "
});
formatter.step({
  "name": "Enter the URL",
  "keyword": "And "
});
formatter.step({
  "name": "Click on ADFS",
  "keyword": "And "
});
formatter.step({
  "name": "Enter userName as \u003cUserName\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "Enter password as \u003cPassword\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "click on Login",
  "keyword": "When "
});
formatter.step({
  "name": "user Login successfully",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "UserName",
        "Password"
      ]
    },
    {
      "cells": [
        "lrampra3",
        "Oct@2018_"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Login to OMCC2",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Open the Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDetails.open_the_Browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Maximize the browser",
  "keyword": "And "
});
formatter.match({
  "location": "StepDetails.maximize_the_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the URL",
  "keyword": "And "
});
formatter.match({
  "location": "StepDetails.enter_the_URL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on ADFS",
  "keyword": "And "
});
formatter.match({
  "location": "StepDetails.click_on_ADFS()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter userName as lrampra3",
  "keyword": "And "
});
formatter.match({
  "location": "StepDetails.enter_userName_as_lrampra(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter password as Oct@2018_",
  "keyword": "And "
});
formatter.match({
  "location": "StepDetails.enter_password_as_Oct__(Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Login",
  "keyword": "When "
});
formatter.match({
  "location": "StepDetails.click_on_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user Login successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDetails.user_Login_successfully()"
});
formatter.result({
  "status": "passed"
});
});