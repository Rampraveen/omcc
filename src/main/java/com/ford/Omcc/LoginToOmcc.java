package com.ford.Omcc;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
//import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class LoginToOmcc {

	
	public static WebDriver driver;
	@Parameters({"url","UserName","Password","OrderNumber"})
	@Test
		public void startApp(String url1,String uname, String pwd, String orderNum) throws InterruptedException
		{
			System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer32.exe");
			driver = new InternetExplorerDriver();
		/*System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();*/	
			driver.manage().window().maximize();
			driver.get(url1);
			Thread.sleep(10000);
			driver.findElement(By.id("ADloginStrongAuthRef")).click();
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//a[text()='OMCC Silverlight Application']")).click();
			Thread.sleep(20000);
/*			driver.findElement(By.xpath("//input[@name='userid']")).click();
			Thread.sleep(5000);*/
			driver.findElement(By.id("DEALER-WSLXloginUserIdInput")).sendKeys(uname);
			Thread.sleep(1000);
			driver.findElement(By.id("DEALER-WSLXloginPasswordInput")).sendKeys(pwd);
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[@value='Login']")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("DEALER-WSLXauthSuccessContinueButtonText")).click();
			Actions act = new Actions(driver);
			//in pre-homepage
			driver.findElement(By.xpath("//span[@id='loggedInUserWelcome']")).click();
			act.sendKeys(Keys.TAB).perform();//1
			act.sendKeys(Keys.TAB).perform();//2
			act.sendKeys(Keys.TAB).perform();//3
			act.sendKeys(Keys.TAB).perform();//4
			act.sendKeys(Keys.TAB).perform();//5
			act.sendKeys(Keys.ARROW_RIGHT).perform();//1 - Selecting market
			act.sendKeys(Keys.ARROW_RIGHT).perform();//2 - Selecting market
			act.sendKeys(Keys.ARROW_RIGHT).perform();//3 - Selecting market
			act.sendKeys(Keys.ARROW_RIGHT).perform();//4 - Selecting market
			act.sendKeys(Keys.ARROW_RIGHT).perform();//5 - Selecting market
			act.sendKeys(Keys.TAB).perform();// - Selecting market
			act.sendKeys(Keys.SPACE).perform();// - Selecting market
			act.sendKeys(Keys.TAB).perform();//Selecting language
			act.sendKeys(Keys.TAB).perform();//Selecting language
			act.sendKeys(Keys.SPACE).perform();//Selecting language
			act.sendKeys(Keys.TAB).perform(); //Selecting brand
			act.sendKeys(Keys.TAB).perform();//Selecting brand
			act.sendKeys(Keys.ARROW_DOWN).perform();//Selecting brand
			act.sendKeys(Keys.TAB).perform();//Selecting brand
			act.sendKeys(Keys.SPACE).perform();//Selecting Brand
			act.sendKeys(Keys.TAB).perform();
			act.sendKeys(Keys.SPACE).perform();
			
			//driver.switchTo().
			//driver.quit();
			/*driver.findElementById("ADloginStrongAuthRef").click();
			Thread.sleep(3000);
			driver.findElementByXPath("//input[@name='userid']").click();
			Thread.sleep(5000);
			driver.findElementById("DEALER-WSLXloginUserIdInput").sendKeys("lrampra3");
			Thread.sleep(1000);
			driver.findElementById("DEALER-WSLXloginPasswordInput").sendKeys("Oct@2018_");
			driver.quit();*/
			
			//in home page
			driver.findElement(By.xpath("//span[@id='loggedInUserWelcome']")).click();
			act.sendKeys(Keys.TAB).perform();//1 - Search Order button
			act.sendKeys(Keys.TAB).perform();//2 - Search Order button
			act.sendKeys(Keys.TAB).perform();//3 - Search Order button
			act.sendKeys(Keys.TAB).perform();//4 - Search Order button
			act.sendKeys(Keys.TAB).perform();//5 - Search Order button
			act.sendKeys(Keys.TAB).perform();//6 - Search Order button
			act.sendKeys(Keys.SPACE).perform(); // Search Order button
			driver.findElement(By.xpath("//span[@id='loggedInUserWelcome']")).click();
			act.sendKeys(Keys.TAB).perform();//1
			act.sendKeys(Keys.TAB).perform();//2
			act.sendKeys(Keys.TAB).perform();//3
			act.sendKeys(Keys.TAB).perform();//4
			act.sendKeys(Keys.TAB).perform();//5
			act.sendKeys(Keys.TAB).perform();//6
			act.sendKeys(orderNum).perform();
			act.sendKeys(Keys.RETURN).perform();
			act.sendKeys(Keys.TAB).perform();
			act.sendKeys(Keys.SPACE).perform();
		}
	
	
}
