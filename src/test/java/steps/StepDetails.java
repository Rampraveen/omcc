package steps;

import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDetails {
	
	public static WebDriver driver;
	@Given("Open the Browser")
	public void open_the_Browser() {
	    
		System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer32.exe");
		driver = new InternetExplorerDriver();
	    
	}

	@Given("Maximize the browser")
	public void maximize_the_browser() {
		driver.manage().window().maximize();
	    
	}

	@Given("Enter the URL")
	public void enter_the_URL() {
		driver.get("https://wwwedu.gotd-omcc-ist.ford.com/GotdOmccUiWeb/home.faces?windowId=149");
	    
	}

	@Given("Click on ADFS")
	public void click_on_ADFS() {
	    
	    
	}

	@Given("Enter userName as lrampra{int}")
	public void enter_userName_as_lrampra(Integer int1) {
	    
	    
	}

	@Given("Enter password as Oct@{int}_")
	public void enter_password_as_Oct__(Integer int1) {
	    
	    
	}

	@When("click on Login")
	public void click_on_Login() {
	    
	    
	}

	@Then("user Login successfully")
	public void user_Login_successfully() {
	    
	    
	}


}
